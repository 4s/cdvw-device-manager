
Cordova plugin wrapper for the Device manager
=============================================

This wrapper packages and builds the platform-independent Device
manager module.


Documentation
-------------

To add Bluetooth support to a Cordova-based application, issue the
following command in the application folder:
```
cordova plugin add https://bitbucket.org/4s/cdvw-device-manager.git
```



Issue tracking
--------------

If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.


License
-------

The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0
